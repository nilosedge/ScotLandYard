package net.nilosplace;

import java.util.ArrayList;
import java.util.HashMap;

import net.nilosplace.Common.Board;
import net.nilosplace.Common.RouteType;

public class Test {

	public static void main(String[] args) {
		new Test();
	}

	public Test() {
		Board b = new Board(0);

		ArrayList<RouteType> list = new ArrayList<RouteType>();
		list.add(RouteType.TAXI);
		list.add(RouteType.BUS);
		list.add(RouteType.UNDERGROUND);
		list.add(RouteType.RIVER);

		System.out.println();
		System.out.println("Path: " + b.shortestPath(194, 108, list));
		
		int longest = 0;
		for(int i = 1; i < 200; i++) {
			for(int k = 1; k < 200; k++) {
				ArrayList<Integer> moves = b.shortestPath(i, k, list);
				
				if(moves.size() > longest) {
					System.out.println(i + " -> " + k + " : " + moves.size());
					System.out.println(moves);
					longest = moves.size();
					//b.validateMove(i, k, RouteType.TAXI);
					//b.shortestPath(i, k, list);
					//b.getMoves(i);
				}
			}
		}
	}
}
