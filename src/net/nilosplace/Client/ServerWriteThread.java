package net.nilosplace.Client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

import net.nilosplace.Common.Messages.Message;

public class ServerWriteThread extends Thread {
	
	private Socket server = null;
	private ObjectOutputStream oos = null;
	
	private Semaphore sendSem = new Semaphore(1);
	private LinkedList<Message> sendQueue = new LinkedList<Message>();

	public ServerWriteThread(ServerHandler serverHandler, Socket server) {
		this.server = server;
		try {
			oos = new ObjectOutputStream(this.server.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void run() {
		while(true) {
			try {
				sendSem.acquire();
				if(!sendQueue.isEmpty()) {
					Message m = sendQueue.remove();
					sendSem.release();
					//System.out.println("Sending message to server: ");
					//m.print();
					oos.writeObject(m);
					oos.flush();
					oos.reset();
				} else {
					sendSem.release();
					sleep(100);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
		}
	}

	public void sendMessageToClient(Message m) {
		try {
			sendSem.acquire();
			sendQueue.add(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(0);
		}
		sendSem.release();
	}
}
