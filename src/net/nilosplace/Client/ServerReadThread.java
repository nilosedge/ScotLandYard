package net.nilosplace.Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import net.nilosplace.Common.Messages.Message;

public class ServerReadThread extends Thread {
	
	private Socket server = null;
	private ServerHandler hand = null;
	private ObjectInputStream ois = null;

	public ServerReadThread(ServerHandler serverHandler, Socket server) {
		hand = serverHandler;
		this.server = server;
		try {
			ois = new ObjectInputStream(this.server.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void run() {
		while(true) {
			Message m;
			try {
				m = (Message)ois.readObject();
				hand.sendMessageToEngine(m);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
		}
	}
}
