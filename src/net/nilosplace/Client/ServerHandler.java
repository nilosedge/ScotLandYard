package net.nilosplace.Client;

import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

import net.nilosplace.Common.Messages.Message;

public class ServerHandler extends Thread {
	
	private ClientEngine engine = null;

	private ServerReadThread read = null;
	private ServerWriteThread write = null;
	
	private Semaphore writeSem = new Semaphore(1);
	private Semaphore readSem = new Semaphore(1);
	private LinkedList<Message> writeQueue = new LinkedList<Message>();
	private LinkedList<Message> readQueue = new LinkedList<Message>();

	public ServerHandler(Socket server) {
		write = new ServerWriteThread(this, server);
		read = new ServerReadThread(this, server);
		write.start();
		read.start();
	}
	
	public void run() {
		boolean work1 = false;
		boolean work2 = false;
		while(true) {
			try {
				readSem.acquire();
				if(!readQueue.isEmpty()) {
					Message m = readQueue.remove();
					work1 = true;
					readSem.release();
					engine.sendMessageToEngine(m);
				} else {
					work1 = false;
					readSem.release();
				}
				writeSem.acquire();
				if(!writeQueue.isEmpty()) {
					Message m = writeQueue.remove();
					work2 = true;
					writeSem.release();
					write.sendMessageToClient(m);
				} else {
					work2 = false;
					writeSem.release();
				}
				if(!work1 && !work2) {
					sleep(100);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void sendMessageToServer(Message m) {
		try {
			writeSem.acquire();
			writeQueue.add(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		writeSem.release();
	}
	
	public void sendMessageToEngine(Message m) {
		try {
			readSem.acquire();
			readQueue.add(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		readSem.release();
	}

	public void setEngine(ClientEngine engine) {
		this.engine = engine;
		this.start();
		engine.start();
	}
}
