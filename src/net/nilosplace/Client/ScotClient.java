package net.nilosplace.Client;

import java.net.Socket;

import net.nilosplace.Engines.AIClientEngine;
import net.nilosplace.Engines.CommandLineClientEngine;

public class ScotClient extends Thread {
	
	private Socket server;
	private ServerHandler handler = null;

	public ScotClient(String clientType) {
		try {
			server = new Socket("localhost", 12345);
			handler = new ServerHandler(server);
			if(clientType.equals("command")) {
				handler.setEngine(new CommandLineClientEngine(handler));
			}
			if(clientType.equals("ai")) {
				handler.setEngine(new AIClientEngine(handler));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		while(true) {
			try {
				if(handler.isAlive()) {
					sleep(1000);
				} else {
					System.exit(0);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		for(String s: args) {
			System.out.println("String: " + s);
		}
		System.out.println(args.length);
		if(args.length == 0) {
			System.out.println("Please choose a client to run: ");
			System.out.println("[command, ai]");
			System.exit(1);
		}
		ScotClient p = new ScotClient(args[0]);
		p.start();
	}

}
