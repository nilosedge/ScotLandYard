package net.nilosplace.Client.AI;

import java.util.ArrayList;
import java.util.HashMap;

import net.nilosplace.Common.Board;
import net.nilosplace.Common.Player;
import net.nilosplace.Common.RouteType;
import net.nilosplace.Common.Messages.InitGameMessage;
import net.nilosplace.Common.Messages.PlayerMoveMessage;
import net.nilosplace.Common.Messages.PlayerMovedMessage;
import net.nilosplace.Common.Messages.StartGameMessage;

public abstract class AIPlayer extends Player {

	protected Board board = new Board(10);
	protected ArrayList<RouteType> detectiveRouteTypes = RouteType.getDetectiveRouteTypes();
	//protected ArrayList<TicketType> detectedTicketTypes = TicketType.getDetectiveTicketTypes();
	
	protected int myPlayerId = 0;
	// <Player, Position>
	protected HashMap<Integer, Integer> playerPositions = new HashMap<Integer, Integer>();
	
	//this.playerPositions = playerId.getPositions();

	public AIPlayer(InitGameMessage message) {
		super(message.getClientId());
		setMrx(message.isMrx());
		this.myPlayerId = message.getClientId();
		playerPositions.put(myPlayerId, message.getInitialPlayerPosition());
	}
	
	public HashMap<Integer, Integer> getPlayerPositions() {
		return playerPositions;
	}

	public void setPlayerPositions(HashMap<Integer, Integer> playerPositions) {
		playerPositions.put(myPlayerId, this.playerPositions.get(myPlayerId));
		this.playerPositions = playerPositions;
	}

	public abstract void playerMoved(PlayerMovedMessage message);
	
	public int getMyPlayerId() {
		return myPlayerId;
	}

	public abstract PlayerMoveMessage getNextMove();
	public abstract void startGame(StartGameMessage message);

}
