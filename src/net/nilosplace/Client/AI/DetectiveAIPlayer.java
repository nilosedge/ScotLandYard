package net.nilosplace.Client.AI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import net.nilosplace.Common.RouteType;
import net.nilosplace.Common.TicketType;
import net.nilosplace.Common.Messages.InitGameMessage;
import net.nilosplace.Common.Messages.PlayerMoveMessage;
import net.nilosplace.Common.Messages.PlayerMovedMessage;
import net.nilosplace.Common.Messages.StartGameMessage;

public class DetectiveAIPlayer extends AIPlayer {

	private List<Integer> startPos = Arrays.asList(13, 26, 29, 34, 50, 53, 91, 94, 103, 112, 117, 132, 138, 141, 155, 174, 197, 198);
	
	private HashMap<Integer, ArrayList<Integer>> playersMoves = new HashMap<Integer, ArrayList<Integer>>();
	private ArrayList<TicketType> mrxTickets = new ArrayList<TicketType>();
	
	public DetectiveAIPlayer(InitGameMessage message) {
		super(message);
	}
	
	@Override
	public void playerMoved(PlayerMovedMessage message) {
		playerPositions.put(message.getPlayerThatMoved(), message.getNewPosition());
		// TODO keep list of mrx positions
		System.out.println("Player Positions: " + playerPositions);
		
		playersMoves.get(message.getPlayerThatMoved()).add(message.getNewPosition());
		
		for(Integer player: playersMoves.keySet()) {
			ArrayList<Integer> moves = playersMoves.get(player);
			System.out.println("Player: " + player + " Moves: " + moves);
		}

		if(message.isMrx()) {
			System.out.println("Mrx Moved: " + message.getTicketType());
			mrxTickets.add(message.getTicketType());
			System.out.println("Tickets: " + mrxTickets);

		}
		possiableMrxLocations();


	}

	private void possiableMrxLocations() {
		
//		if(mrxTickets.size() == 0) {
//			
//			
//		} else {
//			
//			
//		}
		
		ArrayList<Integer> positions = new ArrayList<Integer>();
		positions.addAll(startPos);

		for(int turn = 0; turn < playersMoves.get(1).size(); turn++) {
			if(turn == 0) {
				for(int p: playerPositions.keySet()) {
					positions.removeAll(Arrays.asList(p));
				}
				
			} else {
				ArrayList<TicketType> ticket = new ArrayList<TicketType>();
				ticket.add(mrxTickets.get(turn - 1));
				
				for(Integer pos: positions) {
					HashMap<TicketType, ArrayList<Integer>> moves = board.getMoves(pos, ticketTypes());
					for(TicketType ticketType: moves.keySet()) {
						
					}
					// remove previous and current locations from players
					
				}
				
				System.out.println("Possible Postitions: " + positions);
			}
		}
		
	}

	@Override
	public PlayerMoveMessage getNextMove() {
		
		HashMap<TicketType, ArrayList<Integer>> moves = board.getMoves(playerPositions.get(myPlayerId), ticketTypes());
		// TODO Better logic to figure out how to move
		
		for(TicketType t: moves.keySet()) {
			for(Integer i: moves.get(t)) {
				PlayerMoveMessage pmm = new PlayerMoveMessage();
				pmm.setTo(i);
				pmm.setTicketType(t);
				return pmm;
			}
		}
		return null;
	}

	@Override
	public void startGame(StartGameMessage message) {
		for(int player: playerPositions.keySet()) {
			if(playersMoves.get(player) == null) {
				playersMoves.put(player, new ArrayList<Integer>());
			}
			playersMoves.get(player).add(playerPositions.get(player));
		}
	}

}
