package net.nilosplace.Client.AI;

import java.util.ArrayList;
import java.util.HashMap;

import net.nilosplace.Common.RouteType;
import net.nilosplace.Common.TicketType;
import net.nilosplace.Common.Messages.InitGameMessage;
import net.nilosplace.Common.Messages.PlayerMoveMessage;
import net.nilosplace.Common.Messages.PlayerMovedMessage;
import net.nilosplace.Common.Messages.StartGameMessage;

public class MrxAIPlayer extends AIPlayer {
	
	public MrxAIPlayer(InitGameMessage message) {
		super(message);
	}

	@Override
	public void playerMoved(PlayerMovedMessage message) {
		if(message.getPlayerThatMoved() != myPlayerId) {
			playerPositions.put(message.getPlayerThatMoved(), message.getNewPosition());
		}
		System.out.println("Positions: " + this.playerPositions);
	}

	@Override
	public PlayerMoveMessage getNextMove() {
		HashMap<TicketType, ArrayList<Integer>> moves = board.getMoves(playerPositions.get(myPlayerId), ticketTypes());
		// TODO find path that is farthest from all the players and more there
		
		System.out.println("Moves: " + moves);
		
		int bestLocation = 0;
		TicketType bestLocationType = TicketType.UNKNOWN;
		int sum = 0;
		int lastSum = 0;
		for(TicketType ticketType: moves.keySet()) {
			
			for(Integer moveLocation: moves.get(ticketType)) {
				
				sum = 0;
				for(Integer player: playerPositions.keySet()) {
					ArrayList<Integer> path = board.shortestPath(playerPositions.get(player), moveLocation, detectiveRouteTypes);
					if(player != myPlayerId) {
						sum += path.size() - 1;
						System.out.println("Player: " + player + " loc: " + playerPositions.get(player) + " shortest: " + board.shortestPath(playerPositions.get(player), moveLocation, detectiveRouteTypes) + " to: " + moveLocation);
					}
				}
				System.out.println("Score: " + sum);
				if(sum > lastSum) {
					bestLocationType = ticketType;
					bestLocation = moveLocation;
					lastSum = sum;
				}
				System.out.println();
			}
		}
		
		PlayerMoveMessage pmm = new PlayerMoveMessage();
		pmm.setTo(bestLocation);
		pmm.setTicketType(bestLocationType);
		
		playerPositions.put(myPlayerId, bestLocation);
		
		return pmm;
	}

	@Override
	public void startGame(StartGameMessage message) {
		
		
	}
}
