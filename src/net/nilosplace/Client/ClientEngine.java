package net.nilosplace.Client;

import net.nilosplace.Common.Messages.Message;

public abstract class ClientEngine extends Thread {
	
	protected ServerHandler serverHandler = null;
	
	public ClientEngine(ServerHandler serverHandler) {
		this.serverHandler = serverHandler;
	}
	
	public void sendMessageToServer(Message m) {
		serverHandler.sendMessageToServer(m);
	}
	
	public void sendMessageToEngine(Message m) {
		processMessage(m);
	}
	
	public abstract void processMessage(Message m);
	public abstract void run();
}
