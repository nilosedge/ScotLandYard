package net.nilosplace.Common.Messages;

import net.nilosplace.Common.RouteType;
import net.nilosplace.Common.TicketType;

public class PlayerMoveMessage extends Message {
	
	private int to;
	private TicketType ticketType;

	public int getTo() {
		return to;
	}
	public void setTo(int to) {
		this.to = to;
	}
	public TicketType getTicketType() {
		return ticketType;
	}
	public void setTicketType(TicketType ticketType) {
		this.ticketType = ticketType;
	}
	
	
	@Override
	public void printMessage() {
		System.out.println("PlayerMoveMessage: Trying to move to: " + to + " via ticketType: " + ticketType);
	}


}
