package net.nilosplace.Common.Messages;

public class InitGameMessage extends Message {

	private boolean mrx;
	private int initialPlayerPosition = 0;
	
	@Override
	public void printMessage() {
		System.out.println("Initialized Game");
	}

	public boolean isMrx() {
		return mrx;
	}
	public void setMrx(boolean mrx) {
		this.mrx = mrx;
	}
	public int getInitialPlayerPosition() {
		return initialPlayerPosition;
	}
	public void setInitialPlayerPosition(int initialPlayerPosition) {
		this.initialPlayerPosition = initialPlayerPosition;
	}
}
