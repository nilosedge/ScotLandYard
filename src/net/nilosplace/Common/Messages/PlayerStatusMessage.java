package net.nilosplace.Common.Messages;

import java.util.HashMap;

import net.nilosplace.Common.Player;
import net.nilosplace.Common.TicketType;

public class PlayerStatusMessage extends Message {

	private boolean mrx;
	private int playerId;
	private int playerPosition;
	private HashMap<TicketType, Integer> tickets;

	public PlayerStatusMessage() {
		
	}
	
	public PlayerStatusMessage(Player current) {
		setClientId(current.playerId);
		setMrx(current.isMrx());
		setPlayerId(current.playerId);
		setPlayerPosition(current.playerPos);
		setTickets(tickets);
	}

	@Override
	public void printMessage() {
		System.out.println("Getting Player Status");
		System.out.println("Player Id: " + getPlayerId());
		System.out.println("Player Position: " + getPlayerPosition());
		System.out.println("Tickets: " + getTickets());
		System.out.println("Mrx: " + isMrx());
	}
	
	public int getPlayerId() {
		return playerId;
	}
	public boolean isMrx() {
		return mrx;
	}
	public void setMrx(boolean mrx) {
		this.mrx = mrx;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	public int getPlayerPosition() {
		return playerPosition;
	}
	public void setPlayerPosition(int playerPosition) {
		this.playerPosition = playerPosition;
	}
	public HashMap<TicketType, Integer> getTickets() {
		return tickets;
	}
	public void setTickets(HashMap<TicketType, Integer> tickets) {
		this.tickets = tickets;
	}
}
