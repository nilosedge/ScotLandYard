package net.nilosplace.Common.Messages;

import net.nilosplace.Common.TicketType;

public class PlayerMovedMessage extends Message {

	private boolean mrx = false;
	private boolean reveal = false;
	private int newPosition = 0;
	private TicketType ticketType = TicketType.UNKNOWN;
	private int nextPlayersTurn = 0;
	private int playerThatMoved = 0;

	public boolean isMrx() {
		return mrx;
	}
	public void setMrx(boolean mrx) {
		this.mrx = mrx;
	}
	public int getNewPosition() {
		return newPosition;
	}
	public void setNewPosition(int newPosition) {
		this.newPosition = newPosition;
	}
	public TicketType getTicketType() {
		return ticketType;
	}
	public void setTicketType(TicketType ticketType) {
		this.ticketType = ticketType;
	}
	public int getNextPlayersTurn() {
		return nextPlayersTurn;
	}
	public void setNextPlayersTurn(int nextPlayersTurn) {
		this.nextPlayersTurn = nextPlayersTurn;
	}
	public boolean isReveal() {
		return reveal;
	}
	public void setReveal(boolean reveal) {
		this.reveal = reveal;
	}
	public int getPlayerThatMoved() {
		return playerThatMoved;
	}
	public void setPlayerThatMoved(int playerThatMoved) {
		this.playerThatMoved = playerThatMoved;
	}

	@Override
	public void printMessage() {
		System.out.print("Player Moved to: " + newPosition);
		System.out.print(" Reveal Mrx: " + reveal);
		System.out.print(" Player Ticket type: " + ticketType);
		System.out.print(" Player was mrx: " + mrx);
		System.out.println(" Next Players turn: " + nextPlayersTurn);

	}

}
