package net.nilosplace.Common.Messages;

import java.util.ArrayList;
import java.util.HashMap;

import net.nilosplace.Common.TicketType;

public class PlayerMovesMessage extends Message {

	private HashMap<TicketType, ArrayList<Integer>> moves = new HashMap<TicketType, ArrayList<Integer>>();
	
	public void setMove(HashMap<TicketType, ArrayList<Integer>> moves) {
		this.moves = moves;
	}
	
	@Override
	public void printMessage() {
		System.out.println("Players Moves: " + moves);

	}
}
