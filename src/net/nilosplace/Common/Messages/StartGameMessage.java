package net.nilosplace.Common.Messages;

public class StartGameMessage extends Message {

	private int playerToStart = 0;

	
	@Override
	public void printMessage() {
		System.out.println("Game Starts with Player: " + playerToStart);

	}

	public int getPlayerToStart() {
		return playerToStart;
	}
	public void setPlayerToStart(int playersTurn) {
		this.playerToStart = playersTurn;
	}
}
