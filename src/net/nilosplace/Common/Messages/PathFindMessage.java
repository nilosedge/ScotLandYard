package net.nilosplace.Common.Messages;

public class PathFindMessage extends Message {

	private int to;
	private int from;
	
	@Override
	protected void printMessage() {
		System.out.println("Path Message: ");
		System.out.println("Finding path from: " + from + " to: " + to);
	}

	public int getTo() {
		return to;
	}
	public void setTo(int to) {
		this.to = to;
	}
	public int getFrom() {
		return from;
	}
	public void setFrom(int from) {
		this.from = from;
	}
}
