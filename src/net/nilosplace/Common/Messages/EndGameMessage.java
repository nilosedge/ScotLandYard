package net.nilosplace.Common.Messages;

public class EndGameMessage extends Message {

	private int playerWon = 0;

	@Override
	public void printMessage() {
		System.out.println("Game Over Player: " + playerWon + " won");
	}
}
