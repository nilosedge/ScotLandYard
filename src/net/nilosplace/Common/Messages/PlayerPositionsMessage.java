package net.nilosplace.Common.Messages;

import java.util.HashMap;

public class PlayerPositionsMessage extends Message {

	private HashMap<Integer, Integer> positions = new HashMap<Integer, Integer>();

	public void addPlayerPosition(int playerId, int playerPos) {
		positions.put(playerId, playerPos);
	}
	
	public HashMap<Integer, Integer> getPositions() {
		return positions;
	}
	
	@Override
	public void printMessage() {
		System.out.println("Player Positions: " + positions);
	}
}
