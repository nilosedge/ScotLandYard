package net.nilosplace.Common.Messages;

import java.util.ArrayList;

public class PathFoundMessage extends Message {

	ArrayList<Integer> path;

	@Override
	protected void printMessage() {
		System.out.println("Path Found: " + getPath());
		
	}

	public ArrayList<Integer> getPath() {
		return path;
	}
	public void setPath(ArrayList<Integer> path) {
		this.path = path;
	}
}
