package net.nilosplace.Common.Messages;

import java.io.Serializable;

public abstract class Message implements Serializable {

	private static final long serialVersionUID = -7121974095142124461L;

	private int clientId = 0;
	private String error = "";

	public Message() { }

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public int getClientId() {
		return this.clientId;
	}

	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	protected abstract void printMessage();

	public void print() {
		System.out.println(this.getClass().getSimpleName() + ":");
		System.out.println("ClientId: " + clientId);
		if(error.length() > 0) {
			System.out.println("There was an error: " + getError());
		} else {
			printMessage();
		}
		System.out.println();
	}
}
