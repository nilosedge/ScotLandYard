package net.nilosplace.Common;

import java.util.ArrayList;
import java.util.List;

public class EdgeNode {
	
	private List<RouteType> types = null;
	private Vertex vert = null;
	
	public EdgeNode(Vertex v) {
		types = new ArrayList<RouteType>();
		vert = v;
	}

	public Integer getId() {
		return vert.getId();
	}
	
	public boolean hasType(RouteType in) {
		for(RouteType t: types) {
			if(t.name() == in.name()) return true;
		}
		return false;
	}
	
	public void addType(RouteType type) {
		types.add(type);
	}

	public List<RouteType> getTypes() {
		return types;
	}

	public void setTypes(List<RouteType> types) {
		this.types = types;
	}

}
