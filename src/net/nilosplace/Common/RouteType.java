package net.nilosplace.Common;

import java.util.ArrayList;
import java.util.Arrays;


public enum RouteType {
	TAXI(1) { public String toString() { return "1"; } },
	BUS(2) { public String toString() { return "2"; } },
	UNDERGROUND(3) { public String toString() { return "3"; } },
	RIVER(4) { public String toString() { return "4"; } },
	UNKNOWN(5) { public String toString() { return "5"; } };
	private int code;
	private RouteType(int c) {
		code = c;
	}
	public int getCode() {
		return code;
	}
	
	public static RouteType getType(int type) {
		for(RouteType t: RouteType.values()) {
			if(t.getCode() == type) {
				return t;
			}
		}
		return RouteType.UNKNOWN;
	}
	
	public static ArrayList<RouteType> getDetectiveRouteTypes() {
		return new ArrayList<RouteType>(Arrays.asList(TAXI, BUS, UNDERGROUND));
	}
}
	


