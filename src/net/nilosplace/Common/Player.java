package net.nilosplace.Common;

import java.util.ArrayList;
import java.util.HashMap;

public class Player {
	
	public int playerId = 0;
	public int playerPos = 0;
	
	protected boolean mrx = false;
	
	protected HashMap<TicketType, Integer> tickets = new HashMap<TicketType, Integer>();

	public Player(int clientId) {
		this.playerId = clientId;
		tickets.put(TicketType.TAXI, 10);
		tickets.put(TicketType.BUS, 8);
		tickets.put(TicketType.UNDERGROUND, 4);
		tickets.put(TicketType.BLACK, 0);
		tickets.put(TicketType.DOUBLE, 0);
	}

	public boolean isMrx() {
		return mrx;
	}
	public void setMrx(boolean mrx) {
		this.mrx = mrx;
	}
//	public HashMap<TicketType, Integer> getTickets() {
//		return tickets;
//	}
	public void setTickets(HashMap<TicketType, Integer> tickets) {
		this.tickets = tickets;
	}

	public void minus(TicketType ticketType) {
		tickets.put(ticketType, tickets.get(ticketType) - 1);
	}
	public void add(TicketType ticketType) {
		tickets.put(ticketType, tickets.get(ticketType) + 1);
	}

	public ArrayList<TicketType> ticketTypes() {
		ArrayList<TicketType> ret = new ArrayList<TicketType>();
		for(TicketType type: tickets.keySet()) {
			if(tickets(type) > 0) {
				ret.add(type);
			}
		}
		return ret;
	}
	
	public int tickets(TicketType ticket) {
		return tickets.get(ticket);
	}
	public int tickets(TicketType ticket, int amount) {
		return tickets.put(ticket, amount);
	}
}
