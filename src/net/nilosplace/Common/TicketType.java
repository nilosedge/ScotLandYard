package net.nilosplace.Common;

import java.util.ArrayList;

public enum TicketType {

	TAXI(1) { public String toString() { return "Taxi"; } },
	BUS(2) { public String toString() { return "Bus"; } },
	UNDERGROUND(3) { public String toString() { return "Under Ground"; } },
	BLACK(4) { public String toString() { return "Black Move"; } },
	DOUBLE(5) { public String toString() { return "Double Move"; } },
	UNKNOWN(6) { public String toString() { return "Unknown"; } },
	;
	
	private int code;
	private TicketType(int c) {
		code = c;
	}
	public int getCode() {
		return code;
	}
	
	public static TicketType getType(int type) {
		for(TicketType t: TicketType.values()) {
			if(t.getCode() == type) {
				return t;
			}
		}
		return TicketType.UNKNOWN;
	}
	
	public static ArrayList<RouteType> getRouteTypes(TicketType ticket) {
		ArrayList<RouteType> ret = new ArrayList<RouteType>();
		switch (ticket) {
			case TAXI:
				ret.add(RouteType.TAXI);
				break;
			case BUS:
				ret.add(RouteType.BUS);
				break;
			case UNDERGROUND:
				ret.add(RouteType.UNDERGROUND);
				break;
			case BLACK:
				ret.add(RouteType.TAXI);
				ret.add(RouteType.BUS);
				ret.add(RouteType.UNDERGROUND);
				ret.add(RouteType.RIVER);
				break;
			default:
				ret.add(RouteType.UNKNOWN);
		}
		return ret;
	}
}
