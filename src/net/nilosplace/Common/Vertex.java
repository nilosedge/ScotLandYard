package net.nilosplace.Common;

import java.util.HashMap;

public class Vertex {
	
	private Integer id;
	private HashMap<Integer, EdgeNode> edges = null;
	
	public Vertex(Integer v1) {
		id = v1;
		edges = new HashMap<Integer, EdgeNode>();
	}

	public void addEdge(RouteType type, Vertex v) {
		if(edges.containsKey(v.getId())) {
			EdgeNode e = edges.get(v.getId());
			e.addType(type);
		} else {
			EdgeNode e = new EdgeNode(v);
			e.addType(type);
			edges.put(v.getId(), e);
		}
	}
	
	public Integer getId() {
		return id;
	}
	
	public HashMap<Integer,EdgeNode> getEdges() {
		return edges;
	}
}
