package net.nilosplace.Common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Queue;

public class Board {
	
	private HashMap<Integer, Vertex> verts = null;
	private int debug = 0;

	private HashMap<Integer, Boolean> visited = new HashMap<Integer, Boolean>();

	public Board(int debug) {
		verts = new HashMap<Integer, Vertex>();
		this.debug = debug;
		initBoard();
	}
	
	private void addEdge(Integer v1, Integer v2, RouteType type) {
		Vertex vv1 = verts.get(v1);
		if(vv1 == null) vv1 = new Vertex(v1);
		verts.put(v1, vv1);
		Vertex vv2 = verts.get(v2);
		if(vv2 == null) vv2 = new Vertex(v2);
		verts.put(v2, vv2);
		vv1.addEdge(type, vv2);
		vv2.addEdge(type, vv1);
	}
	
	public void printEdges() {
		for(Entry<Integer, Vertex> e: verts.entrySet()) {
			for(Entry<Integer, EdgeNode> e1: e.getValue().getEdges().entrySet()) {
				for(RouteType t: e1.getValue().getTypes()) {
					System.out.println(e.getValue().getId() + " -> " + e1.getValue().getId() + " -> " + t.name());
				}
			}
		}
	}

	public void initBoard() {
		try {
			InputStream is = null;
			
			File f = new File("map");
			if(f.exists()) {
				is = new FileInputStream(new File("map"));
			} else {
				ClassLoader cl = this.getClass().getClassLoader();
	            is = cl.getResourceAsStream("map");
			}
			
			BufferedReader in = new BufferedReader(new InputStreamReader(is));

			String s = "";
			while((s = in.readLine()) != null) {
				String[] ss = s.split(" ");
				addEdge(Integer.parseInt(ss[0]), Integer.parseInt(ss[1]), RouteType.getType(Integer.parseInt(ss[2])));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(debug > 0) printEdges();
	}

	public boolean validateMove(int moveFrom, int moveTo, RouteType type) {
		if(moveFrom == moveTo) return false;
		Vertex v = verts.get(moveFrom);
		HashMap<Integer, EdgeNode> map = v.getEdges();
		EdgeNode e = map.get(moveTo);
		if(e == null) {
			return false;
		} else {
			return e.hasType(type);
		}
	}

	public HashMap<TicketType, ArrayList<Integer>> getMoves(int location, ArrayList<TicketType> ticketTypes) {
		
		Vertex v = verts.get(location);
		HashMap<TicketType, ArrayList<Integer>> map = new HashMap<TicketType, ArrayList<Integer>>();
		
		for(Entry<Integer, EdgeNode> e: v.getEdges().entrySet()) {
			for(TicketType tt: ticketTypes) {
				ArrayList<RouteType> routeTypes = TicketType.getRouteTypes(tt);
				for(RouteType rt: routeTypes) {
					if(e.getValue().getTypes().contains(rt)) {
						if(map.get(tt) == null) {
							map.put(tt, new ArrayList<Integer>());
						}
						map.get(tt).add(e.getKey());
					}
				}
			}
		}
		return map;
	}
	
	public ArrayList<Integer> shortestPath(int from, int to, ArrayList<RouteType> routeTypes) {
		
		ArrayList<Integer> ret = new ArrayList<Integer>();
		
		if(from == to) return ret;
		
		Vertex fromV = verts.get(from);
		Queue<Level> queue = new LinkedList<Level>();
		HashMap<Integer, Integer> path = new HashMap<Integer, Integer>();
		
		queue.add(new Level(0, fromV));
		//System.out.println("Starting with: " + fromV.getId());
		//int counter = 1;
		while(!queue.isEmpty()) {
		
			Level rl = queue.remove();
			//System.out.println("Trying Node: " + rl.vert.getId());
			visited.put(rl.vert.getId(), true);
			
			for(EdgeNode e: rl.vert.getEdges().values()) {

				int edgeNode = e.getId();
				Vertex v = verts.get(edgeNode);
	
				if(edgeNode == to) {
					//System.out.println(from + " -> " + node + " : " + (rl.level+1));
					path.put(edgeNode, rl.vert.getId());
					//System.out.println(path);
					int end = edgeNode;
					do {
						ret.add(0, end);
						edgeNode = path.get(end);
						//System.out.println(node + " -> " + end);
						end = edgeNode;
					} while(edgeNode != from);
					ret.add(0, end);
					visited.clear();
					return ret;
	
				} else {
					if(visited.get(v.getId()) == null) {
						boolean found = false;
						for(RouteType t: e.getTypes()) {
							if(routeTypes.contains(t)) {
								found = true;
								break;
							}
						}
						if(found) {
							path.put(edgeNode, rl.vert.getId());
							//System.out.println("Adding node: " + v.getId() + " to the mix level: " + (rl.level+1));
							visited.put(v.getId(), true);
							queue.add(new Level(rl.level+1, v));
						}
					}
				}

			}
			
		}
		visited.clear();
		
		return ret;
	}

	public class Level {
		public int level;
		public Vertex vert;
		
		public Level(int level, Vertex vert) {
			this.level = level;
			this.vert = vert;
		}
	}
	

}
