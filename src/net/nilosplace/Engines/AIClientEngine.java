package net.nilosplace.Engines;

import net.nilosplace.Client.ClientEngine;
import net.nilosplace.Client.ServerHandler;
import net.nilosplace.Client.AI.AIPlayer;
import net.nilosplace.Client.AI.DetectiveAIPlayer;
import net.nilosplace.Client.AI.MrxAIPlayer;
import net.nilosplace.Common.Board;
import net.nilosplace.Common.Messages.InitGameMessage;
import net.nilosplace.Common.Messages.Message;
import net.nilosplace.Common.Messages.PlayerMoveMessage;
import net.nilosplace.Common.Messages.PlayerMovedMessage;
import net.nilosplace.Common.Messages.PlayerPositionsMessage;
import net.nilosplace.Common.Messages.StartGameMessage;

public class AIClientEngine extends ClientEngine {
	
	private AIPlayer aip;
	
	public AIClientEngine(ServerHandler serverHandler) {
		super(serverHandler);
	}
	
	@Override
	public void run() {
		while(true) {
			try {
				if(serverHandler.isAlive()) {
					sleep(1000);
				} else {
					System.exit(0);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void processMessage(Message m) {

		if(m instanceof InitGameMessage) {
			InitGameMessage message = (InitGameMessage)m;
			if(message.isMrx()) {
				aip = new MrxAIPlayer(message);
			} else {
				aip = new DetectiveAIPlayer(message);
			}
		}
		
		if(m instanceof PlayerPositionsMessage) {
			PlayerPositionsMessage message = (PlayerPositionsMessage)m;
			aip.setPlayerPositions(message.getPositions());
		}
		if(m instanceof PlayerMovedMessage) {
			PlayerMovedMessage message = (PlayerMovedMessage)m;
			message.printMessage();
			aip.playerMoved(message);
			if(aip.getMyPlayerId() == message.getNextPlayersTurn()) {
				PlayerMoveMessage pmm = aip.getNextMove();
				sendMessageToServer(pmm);
			}
		}
		if(m instanceof StartGameMessage) {
			StartGameMessage message = (StartGameMessage)m;
			if(aip.getMyPlayerId() == message.getPlayerToStart()) {
				PlayerMoveMessage pmm = aip.getNextMove();
				sendMessageToServer(pmm);
			} else {
				aip.startGame(message);
			}
		}
		
	}

}
