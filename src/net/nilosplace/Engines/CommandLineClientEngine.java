package net.nilosplace.Engines;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import net.nilosplace.Client.ClientEngine;
import net.nilosplace.Client.ServerHandler;
import net.nilosplace.Common.TicketType;
import net.nilosplace.Common.Messages.Message;
import net.nilosplace.Common.Messages.PathFindMessage;
import net.nilosplace.Common.Messages.PlayerMoveMessage;
import net.nilosplace.Common.Messages.PlayerMovesMessage;
import net.nilosplace.Common.Messages.PlayerStatusMessage;

public class CommandLineClientEngine extends ClientEngine {
	
	public CommandLineClientEngine(ServerHandler serverHandler) {
		super(serverHandler);
	}

	public void run() {
		String line = "";
		InputStreamReader converter = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(converter);

		while(!line.equals("quit")) {
			try {
				line = in.readLine();
				String[] command = line.split(" ");
				if(command[0].equalsIgnoreCase("MOVE") && command.length == 3) {
					PlayerMoveMessage m = new PlayerMoveMessage();
					m.setTo(Integer.parseInt(command[1]));
					// TODO fix this for mrx command line
					m.setTicketType(TicketType.getType(Integer.parseInt(command[2])));
					sendMessageToServer(m);
				}
				if(command[0].equalsIgnoreCase("PATH") && command.length == 3) {
					PathFindMessage m = new PathFindMessage();
					m.setFrom(Integer.parseInt(command[1]));
					m.setTo(Integer.parseInt(command[2]));
					sendMessageToServer(m);
				}
				if(command[0].equalsIgnoreCase("STATUS")) {
					sendMessageToServer(new PlayerStatusMessage());
				}
				if(command[0].equalsIgnoreCase("MOVES")) {
					PlayerMovesMessage m = new PlayerMovesMessage();
					sendMessageToServer(m);
				}
				if(command[0].equals("Q")) {
					
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
		}
		System.exit(0);
	}

	@Override
	public void processMessage(Message m) {
		//System.out.println("Message Recieved from Server");
		m.print();
	}
}
