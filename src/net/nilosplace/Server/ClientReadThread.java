package net.nilosplace.Server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import net.nilosplace.Common.Messages.Message;

public class ClientReadThread extends Thread {
	
	private ClientHandler hand = null;
	private ObjectInputStream ois = null;

	public ClientReadThread(ClientHandler clientHandler, Socket client) {
		hand = clientHandler;
		try {
			ois = new ObjectInputStream(client.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void run() {
		while(true) {
			Message m;
			try {
				m = (Message)ois.readObject();
				hand.sendMessageToEngine(m);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
			
		}
	}
}
