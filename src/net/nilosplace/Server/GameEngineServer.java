package net.nilosplace.Server;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

import net.nilosplace.Common.Messages.Message;
import net.nilosplace.Common.Messages.NewPlayerMessage;
import net.nilosplace.Common.Messages.PathFindMessage;
import net.nilosplace.Common.Messages.PlayerMoveMessage;
import net.nilosplace.Common.Messages.PlayerMovesMessage;
import net.nilosplace.Common.Messages.PlayerStatusMessage;

public class GameEngineServer extends Thread {

	private Semaphore recvSem = new Semaphore(1);
	private LinkedList<Message> recvQueue = new LinkedList<Message>();
	
	private ClientManager clientManager = null;
	private PlayerManager playerManager = new PlayerManager();

	public GameEngineServer(ClientManager clientManager) {
		this.clientManager = clientManager;
		//board.initBoard();
	}

	public void run() {
		while(true) {
			try {
				recvSem.acquire();
				if(!recvQueue.isEmpty()) {
					Message m = recvQueue.remove();
					recvSem.release();
					processMessage(m);
				} else {
					recvSem.release();
					sleep(100);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	private void processMessage(Message m) {
		
		ArrayList<Message> send = new ArrayList<Message>();
		
		if(!playerManager.isGameStarted() && !playerManager.isGameFull()) {
			if(m instanceof NewPlayerMessage) {
				send.add(playerManager.initPlayer((NewPlayerMessage)m));
			}
			if(m instanceof PlayerStatusMessage) {
				send.add(playerManager.playerStatus((PlayerStatusMessage)m));
			}
			if(playerManager.isGameFull()) {
				send.add(playerManager.sendAllPositions());
				send.add(playerManager.startGame());
			}
		} else if(playerManager.isGameStarted() && !playerManager.isGameEnd()) {
			if(m instanceof PlayerMoveMessage) {
				send.add(playerManager.movePlayer((PlayerMoveMessage)m));
			}
			if(m instanceof PlayerStatusMessage) {
				send.add(playerManager.playerStatus((PlayerStatusMessage)m));
			}
			if(m instanceof PlayerMovesMessage) {
				send.add(playerManager.playerMoves((PlayerMovesMessage)m));
			}
			if(m instanceof PathFindMessage) {
				send.add(playerManager.findPath((PathFindMessage)m));
			}
			if(playerManager.isGameEnd()) {
				send.add(playerManager.gameEnd());
			}
		} else {
			System.out.println("Illegal Game state!!!!!!!");
			System.out.println("Should never see this!!!!");
		}
		clientManager.sendMessagesToClient(send);
	}

	public void sendMessageToEngine(Message m) {
		try {
			recvSem.acquire();
			recvQueue.add(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		recvSem.release();
	}
}
