package net.nilosplace.Server;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

import net.nilosplace.Common.Messages.Message;

public class ClientWriteThread extends Thread {
	
	private Socket client = null;
	private ObjectOutputStream oos = null;
	
	private Semaphore sendSem = new Semaphore(1);
	private LinkedList<Message> sendQueue = new LinkedList<Message>();

	public ClientWriteThread(ClientHandler clientHandler, Socket client) {
		this.client = client;
		try {
			oos = new ObjectOutputStream(this.client.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void run() {
		while(true) {
			try {
				sendSem.acquire();
				if(!sendQueue.isEmpty()) {
					Message m = sendQueue.remove();
					sendSem.release();
					oos.writeObject(m);
					oos.flush();
					oos.reset();
				} else {
					sendSem.release();
					sleep(100);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
		}
	}

	public void sendMessageToClient(Message m) {
		try {
			sendSem.acquire();
			sendQueue.add(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(0);
		}
		sendSem.release();
	}
}
