package net.nilosplace.Server;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.concurrent.Semaphore;

import net.nilosplace.Common.Messages.Message;
import net.nilosplace.Common.Messages.NewPlayerMessage;

public class ClientManager extends Thread {
	
	private Semaphore sendSem = new Semaphore(1);
	private Semaphore recvSem = new Semaphore(1);
	private LinkedList<Message> sendQueue = new LinkedList<Message>();
	private LinkedList<Message> recvQueue = new LinkedList<Message>();
	
	private HashMap<Integer, ClientHandler> connectionList = new HashMap<Integer, ClientHandler>();
	private GameEngineServer e = null;
	private int clientCount = 0;
	
	public ClientManager() {
		e = new GameEngineServer(this);
		e.start();
	}

	public void addConnection(Socket client) {
		System.out.println("Adding Client to Connection List");
		clientCount++;
		connectionList.put(clientCount, new ClientHandler(this, client, clientCount));
		NewPlayerMessage m = new NewPlayerMessage();
		m.setClientId(clientCount);
		sendMessageToEngine(m);
	}
	
	public void run() {
		boolean work1 = false;
		boolean work2 = false;
		while(true) {
			try {
				sendSem.acquire();
				if(!sendQueue.isEmpty()) {
					Message m = sendQueue.remove();
					work1 = true;
					sendSem.release();
					System.out.println("Sending Message to Client: ");
					m.print();
					if(m.getClientId() == 0) {
						for(Entry<Integer, ClientHandler> e: connectionList.entrySet()) {
							e.getValue().sendMessageToClient(m);
						}
					} else {
						connectionList.get(m.getClientId()).sendMessageToClient(m);
					}
				} else {
					work1 = false;
					sendSem.release();
				}
				recvSem.acquire();
				if(!recvQueue.isEmpty()) {
					Message m = recvQueue.remove();
					work2 = true;
					recvSem.release();
					System.out.println("Recieving Message from Client: ");
					m.print();
					e.sendMessageToEngine(m);
				} else {
					work2 = false;
					recvSem.release();
				}
				if(!work1 && !work2) {
					sleep(100);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void sendMessageToClient(Message m) {
		try {
			sendSem.acquire();
			sendQueue.add(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sendSem.release();
	}

	public void sendMessageToEngine(Message m) {
		try {
			recvSem.acquire();
			recvQueue.add(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		recvSem.release();
	}

	public void sendMessagesToClient(ArrayList<Message> send) {
		for(Message m: send) {
			sendMessageToClient(m);
		}
	}

}
