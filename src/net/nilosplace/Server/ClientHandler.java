package net.nilosplace.Server;

import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

import net.nilosplace.Common.Messages.Message;

public class ClientHandler extends Thread {
	
	private ClientManager manager = null;
	private ClientReadThread read = null;
	private ClientWriteThread write = null;
	private int clientId = 0;
	
	private Semaphore writeSem = new Semaphore(1);
	private Semaphore readSem = new Semaphore(1);
	private LinkedList<Message> writeQueue = new LinkedList<Message>();
	private LinkedList<Message> readQueue = new LinkedList<Message>();
	

	public ClientHandler(ClientManager clientManager, Socket client, int clientId) {
		manager = clientManager;
		write = new ClientWriteThread(this, client);
		read = new ClientReadThread(this, client);
		this.clientId = clientId;
		this.start();
		write.start();
		read.start();
	}
	
	public void run() {
		boolean work1 = false;
		boolean work2 = false;
		while(true) {
			try {
				readSem.acquire();
				if(!readQueue.isEmpty()) {
					Message m = readQueue.remove();
					work1 = true;
					readSem.release();
					manager.sendMessageToEngine(m);
				} else {
					work1 = false;
					readSem.release();
				}
				writeSem.acquire();
				if(!writeQueue.isEmpty()) {
					Message m = writeQueue.remove();
					work2 = true;
					writeSem.release();
					write.sendMessageToClient(m);
				} else {
					work2 = false;
					writeSem.release();
				}
				if(!work1 && !work2) {
					sleep(100);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void sendMessageToClient(Message m) {
		try {
			writeSem.acquire();
			if(m.getClientId() == 0) m.setClientId(clientId);
			writeQueue.add(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		writeSem.release();
	}
	
	public void sendMessageToEngine(Message m) {
		try {
			readSem.acquire();
			if(m.getClientId() == 0) m.setClientId(clientId);
			readQueue.add(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		readSem.release();
	}

}
