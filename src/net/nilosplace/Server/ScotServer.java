package net.nilosplace.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ScotServer extends Thread {

	private ServerSocket server;
	private ClientManager manager;
	
	public static void main(String argv[]) {
		new ScotServer();
	}
	
	public ScotServer() {
		
		try {
			server = new ServerSocket(12345);
		} catch (IOException e) {
			e.printStackTrace();
		}
		manager = new ClientManager();
		manager.start();
		start();
	}
	
	public void run() {
		while(true) {
			try {
				Socket client = server.accept();
				manager.addConnection(client);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
