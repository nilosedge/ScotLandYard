package net.nilosplace.Server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import net.nilosplace.Common.Board;
import net.nilosplace.Common.Player;
import net.nilosplace.Common.RouteType;
import net.nilosplace.Common.TicketType;
import net.nilosplace.Common.Messages.EndGameMessage;
import net.nilosplace.Common.Messages.InitGameMessage;
import net.nilosplace.Common.Messages.NewPlayerMessage;
import net.nilosplace.Common.Messages.PathFindMessage;
import net.nilosplace.Common.Messages.PathFoundMessage;
import net.nilosplace.Common.Messages.PlayerMoveMessage;
import net.nilosplace.Common.Messages.PlayerMovedMessage;
import net.nilosplace.Common.Messages.PlayerMovesMessage;
import net.nilosplace.Common.Messages.PlayerPositionsMessage;
import net.nilosplace.Common.Messages.PlayerStatusMessage;
import net.nilosplace.Common.Messages.StartGameMessage;

public class PlayerManager {

	private Board board = null;
	private HashMap<Integer, Player> players = new HashMap<Integer, Player>();
	private int[] startPos = {13, 26, 29, 34, 50, 53, 91, 94, 103, 112, 117, 132, 138, 141, 155, 174, 197, 198};
	private int[] revealMoves = {2, 7, 12, 17, 24};
	private int mrxMoveCounter = 0;
	private boolean gameStarted = false;
	private boolean gameFull = false;
	private boolean gameEnd = false;
	private int playersTurn = 0;
	private String errorString = "";
	private Player mrx;
	
	public PlayerManager() {
		board = new Board(0);
	}
	
	public InitGameMessage initPlayer(NewPlayerMessage m) {
		
		Player p = new Player(m.getClientId());
		players.put(m.getClientId(), p);
		// TODO Remove this line to add more players
		if(players.size() == 3) gameFull = true;

		InitGameMessage send = new InitGameMessage();
		send.setClientId(m.getClientId());
		
		if(m.getClientId() == 1) {
			send.setMrx(true);
			p.setMrx(true);
			mrx = p;
			mrx.tickets(TicketType.DOUBLE, 2);
		} else {
			mrx.add(TicketType.BLACK);
		}
		
		p.playerPos = generatePosition();

		send.setInitialPlayerPosition(p.playerPos);
		
		return send;
	}
	
	public PlayerStatusMessage playerStatus(PlayerStatusMessage m) {
		Player current = players.get(m.getClientId());
		PlayerStatusMessage send = new PlayerStatusMessage(current);
		return send;
	}
	
	public PlayerPositionsMessage sendAllPositions() {
		PlayerPositionsMessage send = new PlayerPositionsMessage();
		send.setClientId(0);
		for(Entry<Integer, Player> e: players.entrySet()) {
			Player p = e.getValue();
			if(p.isMrx()) {
				send.addPlayerPosition(p.playerId, 0);
			} else {
				send.addPlayerPosition(p.playerId, p.playerPos);
			}
		}
		return send;
	}
	
	public PlayerMovesMessage playerMoves(PlayerMovesMessage m) {
		Player current = players.get(m.getClientId());
		HashMap<TicketType, ArrayList<Integer>> moves = board.getMoves(current.playerPos, current.ticketTypes());
		
		PlayerMovesMessage send = new PlayerMovesMessage();
		send.setClientId(m.getClientId());
		send.setMove(moves);
		return send;
	}
	
	public StartGameMessage startGame() {

		gameStarted = true;
		playersTurn = 1;
		
		StartGameMessage send = new StartGameMessage();
		send.setClientId(0);
		send.setPlayerToStart(playersTurn);
		
		return send;
	}
	
	public EndGameMessage gameEnd() {
		EndGameMessage send = new EndGameMessage();
		send.setClientId(0);
		// TODO: Player that won
		//send.setPlayerWon(playerId);
		return send;
	}
	
	public PathFoundMessage findPath(PathFindMessage m) {
		Player current = players.get(m.getClientId());
		// TODO put these else where in the code
		
		ArrayList<RouteType> moveTypes = new ArrayList<RouteType>();

		moveTypes.add(RouteType.BUS);
		moveTypes.add(RouteType.TAXI);
		moveTypes.add(RouteType.UNDERGROUND);
		if(current.tickets(TicketType.BLACK) > 0) {
			moveTypes.add(RouteType.RIVER);
		}

		ArrayList<Integer> path = board.shortestPath(m.getFrom(), m.getTo(), moveTypes);
		PathFoundMessage send = new PathFoundMessage();
		send.setPath(path);
		return send;
	}

	public PlayerMovedMessage movePlayer(PlayerMoveMessage m) {
		System.out.println("Player: " + m.getClientId() + " moves to: " + m.getTo());
		Player current = players.get(m.getClientId());
		Player mrx = players.get(1);
		
		PlayerMovedMessage send = new PlayerMovedMessage();
		
		// TODO Check that no other player is on the position
		//return verts.get(playerPos).getEdges().get(moveTo).hasType(Type.valueOf(data2));
		
		if(current.playerId == playersTurn) {
			System.out.println("Validating move: " + current.playerPos + " -> " + m.getTo() + " TicketType: " + m.getTicketType());
			if(makeMove(current, m)) {
				
				send.setTicketType(m.getTicketType());
				send.setPlayerThatMoved(current.playerId);
				send.setNewPosition(current.playerPos);
				
				if(current.isMrx()) {
					
					send.setMrx(true);
					send.setNewPosition(0);
					for(int i = 0; i < revealMoves.length; i++) {
						if(mrxMoveCounter == revealMoves[i]) {
							send.setReveal(true);
							send.setNewPosition(current.playerPos);
							break;
						}
					}
					mrxMoveCounter++;
				}
				
				for(Player p: players.values()) {
					if(!p.isMrx() && p.playerPos == mrx.playerPos) {
						gameEnd = true;
					}
				}

				incPlayerTurnCounter();
				send.setNextPlayersTurn(playersTurn);
				
			} else {
				send.setClientId(m.getClientId());
				send.setError(errorString);
			}

		} else {
			send.setClientId(m.getClientId());
			send.setError("Not your move");
		}
		return send;
	}

	private void incPlayerTurnCounter() {
		if(playersTurn == players.size()) {
			playersTurn = 1;
		} else {
			playersTurn++;
		}
	}

	private boolean makeMove(Player mover, PlayerMoveMessage m) {
		System.out.println("Trying to make move for: " + mover.playerId);
		
		switch (m.getTicketType()) {
		case TAXI:
			if(board.validateMove(mover.playerPos, m.getTo(), RouteType.TAXI)) {
				if(mover.tickets(TicketType.TAXI) > 0) {
					mover.minus(TicketType.TAXI);
					mrx.add(TicketType.TAXI);
				} else {
					errorString = "Taxi Moves is Zero";
					return false;
				}
			} else {
				errorString = "Not a valid board move";
				return false;
			}
			
			break;
		case BUS:
			if(board.validateMove(mover.playerPos, m.getTo(), RouteType.BUS)) {
				if(mover.tickets(TicketType.BUS) > 0) {
					mover.minus(TicketType.BUS);
					mrx.add(TicketType.BUS);
				} else {
					errorString = "Bus Moves is Zero";
					return false;
				}
			} else {
				errorString = "Not a valid board move";
				return false;
			}
			break;
		case UNDERGROUND:
			if(board.validateMove(mover.playerPos, m.getTo(), RouteType.UNDERGROUND)) {
				if(mover.tickets(TicketType.UNDERGROUND) > 0) {
					mover.minus(TicketType.UNDERGROUND);
					mrx.add(TicketType.UNDERGROUND);
				} else {
					errorString = "Underground Moves is Zero";
					return false;
				}
			} else {
				errorString = "Not a valid board move";
				return false;
			}
			break;
		case BLACK:
			if(mover.isMrx()) {
				if(mover.tickets(TicketType.BLACK) > 0) {
					if(
						board.validateMove(mover.playerPos, m.getTo(), RouteType.TAXI) ||
						board.validateMove(mover.playerPos, m.getTo(), RouteType.BUS) ||
						board.validateMove(mover.playerPos, m.getTo(), RouteType.UNDERGROUND) ||
						board.validateMove(mover.playerPos, m.getTo(), RouteType.RIVER)
					) {
						mover.minus(TicketType.BLACK);
					} else {
						errorString = "Not a valid board move";
						return false;
					}
				} else {
					errorString = "Black Moves is Zero";
					return false;
				}
			} else {
				errorString = "You are not allowed to use the black moves";
				return false;
			}
			break;
		case DOUBLE:
			// TODO figure out how to handle this one
			break;
		case UNKNOWN:
			errorString = "Unknown move type";
			return false;
		default:
			break;
		}

		mover.playerPos = m.getTo();
		return true;
	}

	private int generatePosition() {
		boolean found = false;
		while(true) {
			found = false;
			int rand = (int)(Math.random() * startPos.length);
			for(Entry<Integer, Player> e: players.entrySet()) {
				if(e.getValue().playerPos == startPos[rand]) {
					found = true;
				}
			}
			if(!found) {
				return startPos[rand];
			}
		}
	}

	public boolean isGameStarted() {
		return gameStarted;
	}

	public boolean isGameFull() {
		return gameFull;
	}

	public boolean isGameEnd() {
		return gameEnd;
	}


}
